import React, {Component, PureComponent} from 'react';
import ReactDOM, {render} from 'react-dom';
import PropTypes from 'prop-types';

function Line() {
  return (
    <hr style={Style.width}/>
  )
}

// -----
const INTERVAL = 100;

class Timer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: 20995}

    // this.increment = this.increment.bind(this)
  }

  increment = () => {
    this.setState({value: this.state.value + 1});
  }

  componentDidMount() {
    // this.timerID = setInterval(() => this.increment(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render() {
    const value = this.state.value;
    console.clear();
    console.log('totlal ' + value + '------------------------------');
    console.log('value/INTERVAL/60 ' + value + '/' + INTERVAL + '/' + 60 + ' = ' + value / INTERVAL / 60);
    console.log('value/INTERVAL ' + value + '/' + INTERVAL + ' = ' + value / INTERVAL);
    console.log('value % INTERVAL ' + value % INTERVAL);

    return (
      <div>
        <p>Таймер:</p>
        <p>
          <span>{Math.round(value / INTERVAL / 60 / 60)} : </span>
          <span>{Math.round(value / INTERVAL / 60)} : </span>
          <span>{Math.round(value / INTERVAL)} . </span>
          <span>{value % INTERVAL}</span>
        </p>
        <button onClick={this.increment}>{value}</button>
      </div>
    )
  }
}

// -----
class Conditioner extends React.Component {
  constructor(props) {
    super(props)
    this.state = {temperature: 0}

    // 1
    this.onIncrement = this.onIncrement.bind(this);
    this.onDecrement = this.onDecrement.bind(this);
  }

  onIncrement() {
    this.setState(prevState => ({
      temperature: prevState.temperature + 1
    }))
  }

  onDecrement() {
    this.setState(prevState => ({
      temperature: prevState.temperature - 1
    }))
  }

  render() {
    const temperature = this.state.temperature
    return (
      <div>
        <h2>{temperature}</h2>
        {/*1*/}
        <button onClick={this.onDecrement}>-</button>
        <button onClick={this.onIncrement}>+</button>
        {/*2*/}
        {/*<button onClick={(e) => this.onDecrement(e)}>-</button>*/}
        {/*<button onClick={(e) => this.onIncrement(e)}>+</button>*/}

      </div>
    )
  }

}

// -----
class Response extends React.Component {
  render() {
    if (this.props.isSuccess) {
      return (<ErrorMessage/>)
    } else {
      return (<SuccessMessage/>)
    }
  }
}

function ErrorMessage(props) {
  return <h3 style={Style.margin}>Произошла ошибка на сервере. Не удалось сохранить ваши данные.</h3>;
}

function SuccessMessage(props) {
  return <h3>Ваши данные успешно сохранены!</h3>;
}

// -----
class Fire extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isBurning: false};
    this.onSetFire = this.onSetFire.bind(this);
    this.onUnSetFire = this.onUnSetFire.bind(this);
    this.onCheckFire = this.onCheckFire.bind(this);
  }

  onSetFire() {
    this.setState({isBurning: true})
  }

  onUnSetFire() {
    this.setState({isBurning: false})
  }

  onCheckFire() {
    this.setState({isBurning: !this.state.isBurning});
  }

  render() {
    const isBurning = this.state.isBurning;
    let button;
    if (isBurning) {
      button = <UnSetFireButton onClick={this.onUnSetFire}/>
    } else {
      button = <SetFireButton onClick={this.onSetFire}/>
    }
    let check = '';
    if (isBurning) {
      check = 'Потушить'
    } else {
      check = 'Зажечь'
    }


    return (
      <div>
        <Indicator isBurning={isBurning}/>
        {button}
        <button onClick={this.onCheckFire}>{check}</button>
      </div>
    )
  }
}

function Indicator(props) {
  const isBurning = props.isBurning;
  if (isBurning) {
    return <SetFireMessage/>
  } else {
    return <UnSetFireMessage/>
  }
}

function SetFireMessage() {
  return (
    <h3>Камин горит!</h3>
  )
}

function UnSetFireMessage() {
  return (
    <h3>Камин не горит!</h3>
  )
}

function SetFireButton(props) {
  return (<button className="orange" onClick={props.onClick}>Зажечь</button>);
}

function UnSetFireButton(props) {
  return (<button className="blue" onClick={props.onClick}>Потушить</button>);
}

let Style = {
  inline: {
    display: 'flex',
    alignItems: 'center',
  },
  margin: {
    marginRight: '25px'
  },
  displayNone: {
    display: 'none'
  },
  reverse: {
    display: 'flex',
    flexFlow: 'column-reverse'
  },
  width: {
    width: '100%'
  }

};

// -----
function Console(props) {
  const errors = props.errors;
  return (
    <div>
      <h3>Attention!</h3>
      <p>You have <b>{errors.length ? errors.length : 'qwe'}</b> errors.</p>
    </div>
  )

}

const errors = [
  'Failed to load resource: net::ERR_INSECURE_RESPONSE',
  'TypeError: e is undefined',
  'Uncaught ReferenceError: calculate is not defined'
];

// -----
function DangerAlert(props) {
  return (
    <h3>{props.text}</h3>
  )
}

class Application extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isDangerAlertShowed: true};
    this.toggleDangerAlert = this.toggleDangerAlert.bind(this);
  }

  toggleDangerAlert() {
    this.setState({isDangerAlertShowed: !this.state.isDangerAlertShowed})
  }

  render() {
    return (
      <div>
        {
          this.state.isDangerAlertShowed ?
            <DangerAlert text={'qweqweqwee'}/> :
            null
        }
        <button onClick={this.toggleDangerAlert}>click</button>
      </div>
    )
  }
}

// -----

const users = ['Вася', 'Петя', 'Максим', 'Егор'];

function User(props) {
  return (
    <li style={Style.margin} id={props.id}>{props.user} - {'id ' + props.id}</li>
  )
}

function List(props) {

  function getKey(str) {
    // сумма чакродов каждой буквы слова, для id
    let key = 0;
    // console.log('name ', str);
    for (let i = 0; i < str.length; i++) {
      key += str.charCodeAt(i);
      // console.log('letter -', str[i] + ', charCode', str.charCodeAt(i) + ', sumKey', key);

    }
    return key.toString();
  }

  const users = props.users;

  const item = users.map((user) => {
    const key = getKey(user);
    return <User id={key} key={key} user={user}/>
  });

  return (
    <ul style={Style.inline}>
      {item}
    </ul>
  )
}

// -----

function Chat(props) {
  const users = props.users;

  const userList = (
    <p> Пользователи чата:
      {users.map((user) => <b key={user.id}>{user.name} </b>)}
    </p>
  );


  const messagesList = props.messages.map((message) => {
    let author;
    users2.forEach((user) => {
      if (user.id === message.authorId) {
        author = user
      }
    });
    return (
      <div key={message.id}>
        <b>{author.name} </b>
        <span> {message.message} </span>
      </div>
    )


  })
  return (
    <div>
      {userList}
      {messagesList}
    </div>
  )

}

const users2 = [
  {id: 1, name: 'Вася'},
  {id: 2, name: 'Петя'},
  {id: 3, name: 'Ваня'}
];
const messages = [
  {id: 1, message: 'Всем привет!', authorId: 1},
  {id: 2, message: 'И тебе привет!', authorId: 2},
  {id: 3, message: 'Привет, Вася :)', authorId: 3}
];

// -----

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '', password: '', language: 'JavaScript'
    };
    // this.onChangeLogin = this.onChangeLogin.bind(this);
    // this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    // this.onSelectChange = this.onSelectChange.bind(this);
    this.onChangeUniversal = this.onChangeUniversal.bind(this);
  }

  // onChangeLogin(event) {
  //     this.setState({login: event.target.value})
  // }
  //
  // onChangePassword(event) {
  //     this.setState({password: event.target.value})
  // }
  //
  // onSelectChange(event){
  //     this.setState({language: event.target.value})
  // }

  onChangeUniversal(event) {
    const name = event.target.name;
    this.setState({[name]: event.target.value});
  }

  onSubmit(event) {
    console.log('---');
    console.log(`${this.state.login}, добро пожаловать`);
    console.log(`Твой пароль - ${this.state.password}`);
    console.log(`Язык - ${this.state.language}`);
    event.preventDefault();
  }

  render() {
    return (
      <form>
        <input name='login' type="text" onChange={this.onChangeUniversal}/>
        <input name='password' type="text" onChange={this.onChangeUniversal}/>
        <input type="submit" value='Submit' onClick={this.onSubmit}/>
        <label>
          Выберите язык программирования:
          <select name='language' value={this.state.language} onChange={this.onChangeUniversal}>
            <option value="C++">C++</option>
            <option value="Java">Java</option>
            <option value="C#">C#</option>
            <option value="JavaScript">JavaScript</option>
            <option value="Scala">Scala</option>
          </select>
        </label>
      </form>
    )
  }
}

// -----
const UNIT = {
  KPH: 'Км/ч',
  MPH: 'Миль/ч'
};

function convertToKph(mph) {
  return mph * 1.61;
}

function convertToMph(kph) {
  return kph / 1.61;
}

function isSpeedValid(value) {
  if (value !== null && value !== '' && value !== undefined) {
    let intValue = parseInt(value);
    return !(isNaN(intValue) || !isFinite(intValue));
  }
  return 0;
}

function convertSpeed(value, convertor) {
  if (isSpeedValid(value)) {
    const intValue = parseInt(value);
    let converted = convertor(intValue);
    return Math.round(converted * 100) / 100;
  }
  return '';
}

class MainComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {number: '', unit: 'KPH'};
    this.onChangeNumber = this.onChangeNumber.bind(this);
    this.onChangeNumberKph = this.onChangeNumberKph.bind(this);
    this.onChangeNumberMph = this.onChangeNumberMph.bind(this)
  }

  MAX_SPEED_IN_CITY_IN_KPH = 60;

  onChangeNumber(number) {
    this.setState({number})
  }

  onChangeNumberKph(number) {
    this.setState({unit: 'KPH', number})
  }

  onChangeNumberMph(number) {
    this.setState({unit: 'MPH', number})
  }

  render() {
    const unit = this.state.unit;
    const number = this.state.number;
    const kph = unit === 'MPH' ? convertSpeed(number, convertToKph) : number;
    const mph = unit === 'KPH' ? convertSpeed(number, convertToMph) : number;
    return (
      <div>
        <InputComponent unit='KPH' number={kph} onChangeNumber={this.onChangeNumberKph}/>
        <InputComponent unit='MPH' number={mph} onChangeNumber={this.onChangeNumberMph}/>
        <InputDetector speed={kph} maxSpeed={this.MAX_SPEED_IN_CITY_IN_KPH}/>
      </div>
    )
  }
}

class InputComponent extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeInput = this.onChangeInput.bind(this)
  }

  onChangeInput(e) {
    this.props.onChangeNumber(e.target.value)
  }

  render() {
    const unit = this.props.unit;
    return (
      <div>
        <span>--- {UNIT[unit]} ---</span>
        <input type="text" value={this.props.number} onChange={this.onChangeInput} placeholder='test'/>
      </div>
    )
  }

}

function InputDetector(props) {
  if (props.speed > props.maxSpeed) {
    return <p>Превышено</p>
  } else {
    return <p>Не превышено</p>
  }
}

// -----

function SuccessMessage(props) {
  return (
    <div>
      <MessageContent title={props.title}>{props.children}</MessageContent>
    </div>
  )
}

function MessageContent(props) {
  return (
    <div style={Style.inline}>
      <h3 style={Style.margin}>{props.title}</h3>
      <p>{props.children}</p>
    </div>
  )
}

// -----

function ButtonGroup() {
  return (
    <div>
      <ButtonOne>11</ButtonOne>
      <ButtonOne>1111</ButtonOne>
      <ButtonTwo>222222</ButtonTwo>
    </div>
  )
}

function ButtonOne(props) {
  return <button>{props.children}</button>
}

function ButtonTwo(props) {
  return <button>{props.children}</button>
}

// -----


// class ProductCategoryRow extends React.Component {
//     render() {
//         const category = this.props.category;
//         return (
//             <tr>
//                 <th colSpan="2">
//                     {category}
//                 </th>
//             </tr>
//         );
//     }
// }
//
// class ProductRow extends React.Component {
//     render() {
//         const product = this.props.product;
//         const name = product.stocked ?
//             product.name :
//             <span style={{color: 'red'}}>
//         {product.name}
//       </span>;
//
//         return (
//             <tr>
//                 <td>{name}</td>
//                 <td>{product.price}</td>
//             </tr>
//         );
//     }
// }
//
// class ProductTable extends React.Component {
//     render() {
//         const rows = [];
//         let lastCategory = null;
//
//         this.props.products.forEach((product) => {
//             if (product.category !== lastCategory) {
//                 rows.push(
//                     <ProductCategoryRow
//                         category={product.category}
//                         key={product.category} />
//                 );
//             }
//             rows.push(
//                 <ProductRow
//                     product={product}
//                     key={product.name} />
//             );
//             lastCategory = product.category;
//         });
//
//         return (
//             <table>
//                 <thead>
//                 <tr>
//                     <th>Name</th>
//                     <th>Price</th>
//                 </tr>
//                 </thead>
//                 <tbody>{rows}</tbody>
//             </table>
//         );
//     }
// }
//
// class SearchBar extends React.Component {
//     render() {
//         return (
//             <form>
//                 <input type="text" placeholder="Search..." />
//                 <p>
//                     <input type="checkbox" />
//                     {' '}
//                     Only show products in stock
//                 </p>
//             </form>
//         );
//     }
// }
//
// class FilterableProductTable extends React.Component {
//     render() {
//         return (
//             <div>
//                 <SearchBar />
//                 <ProductTable products={this.props.products} />
//             </div>
//         );
//     }
// }


class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.onHandlerCheck = this.onHandlerCheck.bind(this);
    this.onChangeFilterText = this.onChangeFilterText.bind(this);
    this.state = {filterText: '', inStockOnly: false}
  }

  onHandlerCheck() {
    this.setState({inStockOnly: !this.state.inStockOnly})
  }

  onChangeFilterText(text) {
    this.setState({filterText: text})
  }

  render() {

    return (
      <div>
        <SearchBar onChangeText={this.onChangeFilterText} onHandlerCheck={this.onHandlerCheck} state={this.state}/>
        <ProductTable state={this.state} products={this.props.products}/>
      </div>
    )
  }
}

class ProductTable extends React.Component {

  render() {
    const searchText = this.props.state.filterText;
    const products = this.props.products;

    let lastCategory = null;
    const rows = [];
    const filterProduct = [];
    const stockedProduct = this.props.state.inStockOnly;


    // products.forEach((product) => {
    //   if ( product.name.toLowerCase().indexOf(searchText.toLowerCase()) != -1 ) {
    //     filterProduct.push(product);
    //   }
    // });

    function filterProducts(stocked) {
      if (stocked) {
        products.forEach((product) => {
          if (product.name.toLowerCase().indexOf(searchText.toLowerCase()) != -1 && product.stocked) {
            filterProduct.push(product);
          }
        })
      } else {
        products.forEach((product) => {
          if (product.name.toLowerCase().indexOf(searchText.toLowerCase()) != -1) {
            filterProduct.push(product);
          }
        })
      }
      filterTable()
    }

    function filterTable() {

      filterProduct.forEach((product, i) => {
        if (lastCategory !== product.category) {
          rows.push(
            <ProductCategoryRow key={(1 + i) * 12} category={product.category}/>
          )
        }
        rows.push(
          <ProductRow key={(2 + i) * 27} product={product}/>
        );
        lastCategory = product.category;
      });
    }

    filterProducts(stockedProduct);
//     filterProduct.forEach((product, i) => {
//       if ( lastCategory !== product.category ) {
//         rows.push(
//           <ProductCategoryRow key={(1 + i) * 12} category={product.category}/>
//         )
//       }
//       rows.push(
//         <ProductRow key={(2 + i) * 27} product={product}/>
//       );
//       lastCategory = product.category;
//     });

    return (
      <table>
        {/*<tr>{this.props.state.filterText.length}</tr>*/}
        <TableHead/>
        <TableRows rows={rows}/>
      </table>
    )
  }
}

class ProductCategoryRow extends React.Component {
  render() {
    return (
      <tr>
        <th colSpan='2'>
          {this.props.category}
        </th>
      </tr>
    )
  }
}

class ProductRow extends React.Component {
  render() {
    const product = this.props.product;
    const price = product.price;
    const name = product.stocked ?
      product.name :
      <span style={{color: 'red'}}>{product.name}</span>;

    return (
      <tr>
        <td>{name}</td>
        <td>{price}</td>
      </tr>
    )
  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.onClickCheckboxSearchBar = this.onClickCheckboxSearchBar.bind(this);
    this.onChangeFilterTextSearchBar = this.onChangeFilterTextSearchBar.bind(this)
  }

  onClickCheckboxSearchBar() {
    this.props.onHandlerCheck()
  }

  onChangeFilterTextSearchBar(e) {
    this.props.onChangeText(e.target.value)
  }

  render() {
    const props = this.props.state;
    const check = props.inStockOnly;
    return (
      <form>
        <input type="text" value={props.filterText} onChange={this.onChangeFilterTextSearchBar}/>
        <p onClick={this.onClickCheckboxSearchBar}>
          <input type="checkbox" onChange={this.onClickCheckboxSearchBar} checked={check}/>
          {' '}
          Only show products in stock
          {' '}
          {props.filterText}
        </p>
      </form>
    )
  }
}

function TableHead() {
  return (
    <thead>
    <tr>
      <th>Name</th>
      <th>Price</th>
    </tr>
    </thead>
  )
}

function TableRows(props) {
  return (
    <tbody>
    {props.rows}
    </tbody>
  )
}

const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

// -----

const UserJSX = (props) => {
  return (<li>{props.user.name}</li>)
};
UserJSX.propTypes = {
  name: PropTypes.string
}
const UserListJSX = () => {
  const users = [{name: 'Vasya', id: 1}, {name: 'Petya', id: 2}];
  return (<ul>
      {users.map((user) => {
        return <UserJSX key={user.id} user={user} name={user.name}/>
      })}
    </ul>
  )
};

// -----

const UserListPage = (props) => {
  let users = [];
  for (let i = 0; i < props.user.length; i++) {
    users.push(props.children[i]);
  }
  return <ul> {users} </ul>
};
const UserPage = () => {
  const users = [{name: 'Vasya!@#', id: 1}, {name: 'PetyaQEW', id: 2}];
  return <UserListPage user={users}>
    {users.map((user) => {
      return <li key={user.id}>{user.name}</li>
    })}
  </UserListPage>
};

// -----

class CustomTextInput extends React.Component {
  constructor(props) {
    super(props)
    this.textInput = React.createRef();
    this.focusTextInput = this.focusTextInput.bind(this);
  }

  focusTextInput() {
    this.textInput.current.focus()
  }

  render() {
    return (
      <div>
        <input type="text" ref={this.textInput}/>
        <input type="button" value='button' onClick={this.focusTextInput}/>
      </div>
    )
  }

  // refs - callback
  // constructor(props) {
  //     super(props);
  //
  //     this.textInput = null;
  //
  //     this.setTextInputRef = element => {
  //         this.textInput = element;
  //     };
  //
  //     this.focusTextInput = () => {
  //         // Фокусировка на текстовом поле, используя нативный DOM API
  //         if (this.textInput) this.textInput.focus();
  //     };
  // }
  //
  // componentDidMount() {
  //     // автофокус на input при монтировании
  //     this.focusTextInput();
  // }
  //
  // render() {
  //     // Используйте коллбэк для атрибута `ref`, чтобы сохранить
  //     // ссылку на текстовый DOM-элемент input в свойстве экземпляра
  //     // (например this.textInput)
  //     return (
  //         <div>
  //             <input
  //                 type="text"
  //                 ref={this.setTextInputRef}
  //             />
  //             <input
  //                 type="button"
  //                 value="Focus the text input"
  //                 onClick={this.focusTextInput}
  //             />
  //         </div>
  //     );
  // }

}

// -----
class SearchUserForm extends React.Component {
  constructor(props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    console.log(`Name: ${this.inputTextField.value}`)
    event.preventDefault();
  }

  focusTextInput() {
    this.inputTextField.focus();
  }

  render() {
    return (
      <form onClick={this.onSubmit}>
        <label>Имя пользователя:
          <input type="text" ref={(el) => {
            this.inputTextField = el
          }}/></label>
        <input type="submit" value='Submit'/>
        <input type="button" value='focus' onClick={this.focusTextInput.bind(this)}/>
      </form>
    )
  }
}

// -----
class FileInput extends React.Component {
  constructor(props) {
    super(props);
    this.onHandlerSubmit = this.onHandlerSubmit.bind(this);
  }

  onHandlerSubmit(event) {
    event.preventDefault();
    if (this.fileName.files[0]) {
      console.log(`File name - ${this.fileName.files[0].name}`)
    }
  }


  render() {
    return (
      <form>
        <label><input type="file" ref={(el) => this.fileName = el}/></label>
        <br/>
        <input onClick={this.onHandlerSubmit} type="submit" value='Submit'/>
      </form>
    )
  }

}

// -----

class UserList extends React.PureComponent {
  render() {
    return (<strong>{this.props.users.join(', ')}</strong>);
  }
}

class UserAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {users: ['Пользователь 1']};
    this.onAddUser = this.onAddUser.bind(this);
  }

  onAddUser() {
    this.setState(prevState => ({
      users: [...prevState.users, `Пользователь ${this.state.users.length + 1}`]
    }));
  }

  render() {
    return (
      <p>
        <button onClick={this.onAddUser} value="Добавить пользователя">{'Добавить пользователя'}</button>
        <UserList users={this.state.users}/>
      </p>
    );
  }
}

// -----

class ExampleButton extends React.Component {
  constructor(props) {
    super(props);

    this.addToStart = this.addToStart.bind(this);
    this.addToEnd = this.addToEnd.bind(this);
    this.sortByEarliest = this.sortByEarliest.bind(this);
    this.sortByLatest = this.sortByLatest.bind(this);
    this.resetList = this.resetList.bind(this);

    const key = Date.now();
    const date = new Date();
    const counter = 1;

    this.state = {
      list: [
        {
          key: key,
          date: date,
          id: counter
        }
      ],
      counter: counter
    }
  }

  addToStart() {
    let newKey = Date.now();
    let newDate = new Date();
    let newCount = this.state.counter + 1;

    let newElement = [
      {
        key: newKey,
        date: newDate,
        id: newCount
      },
      ...this.state.list
    ];

    this.setState({list: newElement, counter: newCount})
  }

  addToEnd() {
    let newKey = Date.now();
    let newDate = new Date();
    let newCount = this.state.counter + 1;

    let newElement = [
      ...this.state.list,
      {
        key: newKey,
        date: newDate,
        id: newCount
      }
    ];

    this.setState({list: newElement, counter: newCount})
  }

  sortByEarliest() {
    const EarliestArr = this.state.list.sort((a, b) => {
      return a.id - b.id
    });
    this.setState({list: EarliestArr})
  }

  sortByLatest() {
    const LatestArr = this.state.list.sort((a, b) => {
      return b.id - a.id
    });
    this.setState({list: LatestArr})
  }

  resetList() {
    this.setState({list: [], counter: 0})
  }

  render() {
    return (
      <div>
        <button onClick={this.addToStart}>Add New to Start</button>
        <button onClick={this.addToEnd}>Add New to End</button>
        <button onClick={this.sortByEarliest}>Sort by Earliest</button>
        <button onClick={this.sortByLatest}>Sort by Latest</button>
        <button onClick={this.resetList}>Reset List</button>
        <table>
          <thead>
          <tr>
            <th>ID</th>
            <th>{''}</th>
            <th>Created at</th>
          </tr>
          </thead>
          <tbody>
          {this.state.list.map((el) => {
            return <ExampleList key={el.key} {...el} />
          })}
          </tbody>
        </table>
      </div>
    )
  }
}

const ExampleList = props => (
  <tr>
    <td>
      <label>{props.id}</label>
    </td>
    <td>
      <input type='text'/>
    </td>
    <td>
      <label> {props.date.toTimeString()} </label>
    </td>
  </tr>
);

// -----

const themes = {
  light: {
    foreground: '#333333',
    background: '#ffffff',
  },
  dark: {
    foreground: '#ffffff',
    background: '#333333',
  },
};

const ThemeContext = React.createContext(
  themes.dark // значеине по умолчанию
);

class ButtonApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: themes.dark
    };
    this.onToggleTheme = this.onToggleTheme.bind(this);
  };

  onToggleTheme = () => {
    const {dark, light} = themes;
    this.setState(state => ({
      theme: state.theme === dark ? light : dark
    }))
  };

  render() {

    return (
      <div>
        <ThemeContext.Provider value={this.state.theme}>
          <WrapButton onChangeTheme={this.onToggleTheme}/>
        </ThemeContext.Provider>
      </div>
    )

  }

}

const WrapButton = (props) => {
  return (
    <div>
      <ButtonTest onClick={props.onChangeTheme}/>
      <ButtonTestHOC onClick={props.onChangeTheme}/>
    </div>
  )
};

const ButtonTest = (props) => {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <button {...props} style={{background: theme.background, color: theme.foreground}}>Test Button</button>
      )}
    </ThemeContext.Consumer>

  )
};

const ButtonHOC = ({theme, ...props}) => {
  return (
    <button {...props} style={{background: theme.background, color: theme.foreground}}>Test Button HOC</button>
  )
};

function withTheme(Component) {
  return function ThemedComponent(props) {
    return (
      <ThemeContext.Consumer>
        {theme => <Component {...props} theme={theme}/>}
      </ThemeContext.Consumer>
    );
  };
}

const ButtonTestHOC = withTheme(ButtonHOC);

// -----

// https://learn-reactjs.ru/core/portals

const modalRoot = document.getElementById('modal-root');

class Modal extends React.Component {
  constructor(props){
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount(){
    modalRoot.appendChild(this.el);
  }
  componentWillUnmount(){
    modalRoot.removeChild(this.el);
  }

  render(){
    return ReactDOM.createPortal(
      this.props.children,
      this.el,
    )
  }
}

class Parent extends React.Component {
  constructor(props){
    super(props);
    this.state = {clicks: 0};
    this.onHendlerClick = this.onHendlerClick.bind(this)
  }
  onHendlerClick(){
    this.setState(prevProps => ({
      clicks: prevProps.clicks + 1
    }))
  }
  render(){
    return(
      <div onClick={this.onHendlerClick}>
        <p>Число кликов {this.state.clicks}</p>
        <Modal>
          <Child/>
        </Modal>
      </div>
    )
  }
}

const Child = () => {
  return ( <div>
      <button>Click</button>
    </div>
  )
};

// -----

// https://learn-reactjs.ru/core/render-prop

class Cat extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    const mouse = this.props.mouse;
    return(
      <div>
        <p>{mouse.x}, {mouse.y}</p>
        <div style={{position: 'absolute', left: mouse.x, top: mouse.y}}>
          Cat
        </div>
      </div>
    )
  }
}

class Cat2 extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    const mouse = this.props.mouse;
    return (
      <div style={{background: 'blue', color: 'white'}}>
        {mouse.x}
        {' : '}
        {mouse.y}
      </div>
    )
  }
}

class Mouse extends React.Component {
  constructor(props){
    super(props);
    this.state = {x: 0, y: 0};
    this.handleMouseMove = this.handleMouseMove.bind(this)
  }

  handleMouseMove(event){
    this.setState({
      x: event.clientX,
      y: event.clientY
    });
  }
  render(){
    return (
      <div>
        <p>{this.props.title}</p>
        <div style={{ height: '100%', minHeight: '100px', border: '1px solid', position: 'relative'  }} onMouseMove={this.handleMouseMove}>
          {this.props.render(this.state)}
        </div>
      </div>
    )
  }
}

class Mouse2 extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {x: 0, y: 0};
    this.handleMouseMove = this.handleMouseMove.bind(this)
  }

  handleMouseMove(event){
    this.setState({
      x: event.clientX,
      y: event.clientY
    });
  }
  render(){
    return (
      <div>
        <p>{this.props.title}</p>
        <div style={{ height: '100%', minHeight: '100px', border: '1px solid', position: 'relative' }} onMouseMove={this.handleMouseMove}>
          {this.props.render(this.state)}
        </div>
      </div>
    )
  }
}

class MouseTracker extends React.Component {

  // привязка гарантирует, что функция ссылается на ту же самую функцию
  // для PureComponent
  constructor(props){
    super(props);
    this.renderTheCat = this.renderTheCat.bind(this)
  }
  renderTheCat(mouse){
    return <Cat2 mouse={mouse} />
  }
  // ---

  render() {
    return (
      <div >
        <h1>Move the mouse around!</h1>

        <Mouse title='Cat' render={mouse => (
          <Cat mouse={mouse} />
        )}/>

        <Mouse title='Cat2' render={mouse => (
          <Cat2 mouse={mouse}/>
        )} />

        {/*Вариант использования с React.PureComponent*/}
        {/*https://learn-reactjs.ru/core/render-prop*/}
        <Mouse2 title='Cat PureComponent' render={this.renderTheCat} />

        {/*<Mouse render={function(mouse){*/}
        {/*return <Cat mouse={mouse} />*/}
        {/*}}/>*/}

      </div>
    );
  }

}

// -----

class ErrorComponentWrap extends React.Component{
  constructor(props){
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    })
  }
  render() {
    if (this.state.errorInfo) {
      return (
        <div>
          <h2>Something went wrong.</h2>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {this.state.error && this.state.error.toString()}
            <br />
            {this.state.errorInfo.componentStack}
          </details>
        </div>
      );
    }
    return this.props.children;
  }

}

class ErrorComponent extends React.Component{

  constructor(props){
    super(props);
    this.state = {count: 0}
    this.onHandlerClick = this.onHandlerClick.bind(this);
  }

  onHandlerClick() {
    this.setState(prevCount => ({
      count: prevCount.count + 1
    }))
  }

  render() {
    if (this.state.count == 5) {
      throw new Error('I Crached');
    }
    return (
      <div onClick={this.onHandlerClick}>{this.state.count}</div>
    )
  }

}

// -----

function BrickMenu() {
  return (
    <brick-menu class="menu">
      <brick-item selected>Item 1</brick-item>
      <brick-item>Item 2</brick-item>
      <brick-item>Item 3</brick-item>
      <brick-item>Item 4</brick-item>
    </brick-menu>
  );
}

// -----

class ConditionerWrap extends React.Component {
  constructor(props) {
    super(props)
    this.state = {temperature: 0}
    this.onIncrement = this.onIncrement.bind(this);
    this.onDecrement = this.onDecrement.bind(this);
  }

  onIncrement() {
    this.setState(prevState => ({
      temperature: prevState.temperature + 1
    }))
  }

  onDecrement() {
    this.setState(prevState => ({
      temperature: prevState.temperature - 1
    }))
  }

  render() {
    const temperature = this.state.temperature
    return (
      <LogProps onInc={this.onIncrement} onDec={this.onDecrement} temp={temperature} />
    )
  }
}

const ConditionerInner = ({onInc, onDec, temp }) => {
  return (
    <div className={'ConditionerInner'}>
      <h2>{temp}</h2>
      <button onClick={onDec}>-</button>
      <button onClick={onInc}>+</button>
    </div>
  )
}

function logProps (Component){
  // для именования возвразаемого компонента, он не возвращается сразу
  class LogProps extends React.Component{

    componentWillReceiveProps(nextProps){
      console.log('prevProps ', this.props.temp)
      console.log('nextProps ', nextProps.temp);
    }

    render(){
      return (
        <Component {...this.props} />
      )
    }
  }
  // ему даётся имя, Component_name(wrap component_name)
  LogProps.displayName = `LogProps(${Component.displayName || Component.name || 'Component'})`;
  // и после возвращается
  return LogProps;
}

let LogProps = logProps(ConditionerInner);

// -----

const TestContext = React.createContext({
  onClickMe: () => {}
});

class TestContextWrap extends React.Component{
  constructor(props){
    super(props);
    this.state = {count: 0, onClickMe: this.onClickMe }
    this.onClickMe = this.onClickMe.bind(this);
  }

  onClickMe = () => {
    this.setState({count: this.state.count + 1})
  }

  render(){
    return (
      <div>
        <TestContext.Provider  value={this.state}>
          <TestContextCount />
        </TestContext.Provider >
      </div>
    )
  }
}

const TestContextCount = (props) => {
  return (
    <div>
      <TestContext.Consumer>
        {(value) => <button onClick={value.onClickMe} >Click</button>}
      </TestContext.Consumer>
      <TestContext.Consumer>
        {(value) =>  <p>{value.count}</p>}
      </TestContext.Consumer>
    </div>
  )
}


class App extends React.Component {
  render() {
    return (
      <div style={Style.reverse}>
        <div style={{display: 'none'}}>
        <Conditioner/>
        <Line/>
        <div style={Style.inline}>
          <Response isSuccess={true}/>
          <Response isSuccess={false}/>
        </div>
        <Line/>
        <Fire/>
        <Line/>
        <Console errors={errors}/>
        <Line/>
        <Application/>
        <Line/>
        <List users={users}/>
        <Line/>
        <Chat users={users2} messages={messages}/>
        <Line/>
        <LoginForm/>
        <Line/>
        <MainComponent/>
        <Line/>
        <SuccessMessage title='test title'>test children</SuccessMessage>
        <Line/>
        <ButtonGroup/>
        <Line/>
        <FilterableProductTable products={PRODUCTS}/>
        <Line/>
        <Line/>
        <div>
          <UserListJSX/>
          <UserPage/>
        </div>
        <Line/>
        <CustomTextInput/>
        <Line/>
        <SearchUserForm/>
        <Line/>
        <FileInput/>
        <Line/>
        <UserAdmin/>
        <Line/>
        <ExampleButton/>
        <Line/>
        <ButtonApp/>
        <Line/>
        {/*<Parent/>*/}
        <Line/>
        <MouseTracker/>
        <Line/>
        <ErrorComponentWrap>
          <ErrorComponent/>
        </ErrorComponentWrap>
        <Line/>
        <BrickMenu/>
        </div>
        <Line/>
        <ConditionerWrap/>
        <Line/>
        <TestContextWrap/>
      </div>
    )
  }
}

export default App


