import React, {Component} from 'react';
import TaskEditor from '../../components/task-editor';
import TaskList from '../../components/task-list';
import TaskFilter from '../../components/task-filter';
import './style.scss';

export default class TaskApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      filter: 'all',
      editor: '',
      color: '#000000',
      checked: false
    };
    this.onAddTask = this.onAddTask.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onChangeColor = this.onChangeColor.bind(this);
    this.onCheckedTask = this.onCheckedTask.bind(this);
    this.onDeleteTask = this.onDeleteTask.bind(this);
    this.onEditTask = this.onEditTask.bind(this);
    this.onHandlerFilter = this.onHandlerFilter.bind(this);
    this.toggleAll = this.toggleAll.bind(this);
    this.getVisibleTasks = this.getVisibleTasks.bind(this);
    this.onClearComplete = this.onClearComplete.bind(this);
  }

  onAddTask(text) {
    let color = this.state.color;
    let newText = text.trim();

    if (text.length > 0) {

      let newTask = {
        text: newText,
        id: Date.now(),
        isChecked: false,
        color: color
      };
      let newTasks = [...this.state.tasks, newTask];
      this.setState({tasks: newTasks, editor: ''});
    }
    this.updateLocalStorage();
  }

  onChangeInput(e) {
    this.setState({ editor: e.target.value })
  }

  onChangeColor(e) {
    this.setState({color: e.target.value})
  }

  onCheckedTask(id) {
    let taskId = id;
    let newTasks = this.state.tasks.map((task) => {
      if (taskId === task.id) {
        task.isChecked = !task.isChecked
      }
      return task
    });
    this.setState({tasks: newTasks})
  }

  onDeleteTask(id) {
    let taskId = id;
    let newTasks = this.state.tasks.filter((task) => {
      return taskId !== task.id
    });
    this.setState({tasks: newTasks})
  }

  onEditTask(id, text) {
    let taskId = id;
    let newTasks = this.state.tasks.map(task =>
      taskId === task.id ?
        {...task, text: text} :
        task
    );
    this.setState({tasks: newTasks})
  }

  toggleAll() {
    let check = this.state.tasks.every(task => task.isChecked);

    let newArr = this.state.tasks.map((task) => ({
      ...task,
      isChecked: !check
    }));
    this.setState({tasks: newArr});

  }

  onHandlerFilter(type) {
    this.setState({filter: type})
  }

  getVisibleTasks(tasks, status) {

    switch (status) {
      case 'active':
        return tasks.filter((task) => {
          return task.isChecked === false
        });
      case 'completed':
        return tasks.filter((task) => {
          return task.isChecked === true
        });
      default:
        return tasks
    }
  }

  onClearComplete() {
    let newTasks = this.state.tasks.filter(task => {
      return task.isChecked !== true;
    });
    this.setState({tasks: newTasks})
  }

  componentDidMount() {
    let localTasks = JSON.parse(localStorage.getItem('tasks'));
    if (localTasks) {
      this.setState({
        tasks: localTasks
      })
    }
  }

  componentDidUpdate() {
    this.updateLocalStorage();
  }

  updateLocalStorage() {
    const tasks = JSON.stringify(this.state.tasks);
    localStorage.setItem('tasks', tasks);
  }

  render() {
    const arrLength = this.state.tasks.length;
    const completedItem = this.state.tasks.reduce((accum, task) => {
      return task.isChecked ? accum + 1 : accum
    }, 0);
    return (
      <div className='task task__app'>

        <TaskEditor addTask={this.onAddTask}
                    changeColor={this.onChangeColor}
                    changeInput={this.onChangeInput}
                    value={this.state.editor}
                    arrLength={arrLength}
                    completedItem={completedItem}
                    toggleAll={this.toggleAll}/>

        <TaskList   checkedTask={this.onCheckedTask}
                    deleteTask={this.onDeleteTask}
                    editTask={this.onEditTask}
                    changeInput={this.onChangeInput}
                    tasks={this.getVisibleTasks(this.state.tasks, this.state.filter)}/>

        <TaskFilter filterActive={this.state.filter}
                    completedItem={completedItem}
                    arrLength={arrLength}
                    onFilter={this.onHandlerFilter}
                    deleteComplete={this.onClearComplete}/>
      </div>
    )
  }
}