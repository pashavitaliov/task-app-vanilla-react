import React from 'react';
import {render} from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import App from './page/App';
import TaskApp from './page/task-app'

const renderApp = Component => {
  render(
    <AppContainer>
      <Component/>
    </AppContainer>,
    document.getElementById('app')
  );
}

renderApp(TaskApp);

if (module.hot) {
  module.hot.accept('./page/App', () => {
    renderApp(TaskApp);
  });
}
