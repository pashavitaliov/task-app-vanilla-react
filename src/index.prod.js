import React from 'react';
import {render} from 'react-dom';
import App from './page/App';
import TaskApp from './page/task-app'

const renderApp = AppView => {
  render(
      <AppView/>,
    document.getElementById('app')
  );
}

renderApp(TaskApp);
