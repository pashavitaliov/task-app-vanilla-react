import React from 'react';
import PropTypes from 'prop-types';
import TaskTextInput from '../task-text-input';
import TaskColorInput from '../task-color-input';

const TaskEditor = (props) => {

  const {addTask, changeColor} = props;
  
  return (
    <div className='task__editor'>
      <TaskTextInput newTask {...props} onSave={text => {addTask(text)}}/>
      <TaskColorInput changeColor={changeColor}/>
    </div>
  )
};

TaskEditor.propTypes = {
  addTask: PropTypes.func,
  changeColor: PropTypes.func
};

export default TaskEditor