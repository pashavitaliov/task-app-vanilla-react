import React, {Component} from 'react';
import PropTypes from 'prop-types';

class TaskTextInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      text: this.props.text || ''
    };
    this.handlerSubmit = this.handlerSubmit.bind(this);
    this.handlerBlur = this.handlerBlur.bind(this);
    this.handlerChange = this.handlerChange.bind(this)
    this.handlerSubmitBtn = this.handlerSubmitBtn.bind(this);
  }

  handlerSubmit(e) {
    if (e.which === 13) {
      this.props.onSave(e.target.value);
      this.setState({text: ''})
    }
  }

  handlerBlur = e => {
    if (!this.props.newTask) {
      this.props.onSave(e.target.value)
    }
  };

  handlerChange(e) {
    this.setState({text: e.target.value})
  }

  handlerSubmitBtn() {
    this.props.onSave(this.state.text);
    this.setState({text: ''})
  }

  render() {
    const {arrLength, completedItem, toggleAll, newTask} = this.props;
    let addBtn, checkbox;

    if (newTask) {
      addBtn = (
        <button className='task__add-btn' onClick={this.handlerSubmitBtn}>NoteButton</button>
      );
      checkbox = (
        <input type="checkbox" checked={arrLength === completedItem && arrLength !== 0} onChange={toggleAll}/>
      );
    }

    return (
      <div className='task__text-input-wrap'>
        {checkbox}
        <input className='task__text-input'
               autoFocus
               type="text"
               placeholder={'TaskTextInput'}
               value={this.state.text}
               onChange={this.handlerChange}
               onKeyDown={this.handlerSubmit}
               onBlur={this.handlerBlur}/>
        {addBtn}
      </div>
    )
  }
}

TaskTextInput.propTypes = {
  arrLength: PropTypes.number,
  completedItem: PropTypes.number,
  toggleAll: PropTypes.func,
  newTask: PropTypes.bool
};

export default TaskTextInput;