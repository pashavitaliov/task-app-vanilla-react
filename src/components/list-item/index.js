import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import TaskTextInput from '../task-text-input';

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editText: true,
      text: this.props.text || ''
    };
    this.onHandlerEdit = this.onHandlerEdit.bind(this);
    this.onHandlerSave = this.onHandlerSave.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
  }

  onHandlerSave(id, text) {
    if ( text.length === 0 ) {
      this.props.deleteTask(id);
    } else {
      this.props.editTask(id, text)
    }

    this.setState({ editText: true })
  }

  onHandlerEdit() {
    this.setState({ editText: false })
  }

  onChangeInput(e) {
    this.setState({ text: e.target.value })
  }

  render() {
    const { text, isChecked, id, color, checkedTask, deleteTask } = this.props;
    let editing = this.state.editText;
    let element;

    if ( editing ) {
      element = (
        <Fragment>
          <input className='task__checkbox' type="checkbox" checked={isChecked} onChange={checkedTask.bind(null, id)}/>
          <label className={'task__text ' + (isChecked ? 'task__text--complete' : '') } onDoubleClick={this.onHandlerEdit}>{text}</label>
          <button className='task__del-btn' onClick={deleteTask.bind(null, id)}>X</button>
        </Fragment>
      )
    } else {
      element = (
        <div className='task__edit' style={{ marginLeft: '20px' }}>
          <TaskTextInput changeInput={this.onChangeInput}
                         text={text}
                         onSave={(text) => this.onHandlerSave(id, text)}/>
        </div>
      )
    }

    return (
      <li className='task__item' id={id} style={{ color: color }}>
        {element}
      </li>
    )
  }
}

ListItem.propTypes = {
  text: PropTypes.string,
  isChecked: PropTypes.bool,
  id: PropTypes.number,
  color: PropTypes.string,
  checkedTask: PropTypes.func,
  deleteTask: PropTypes.func

};

export default ListItem