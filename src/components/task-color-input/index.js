import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

const TaskColorInput = ({ changeColor }) => {

  return (
    <Fragment>
      <input className='task__color-input' type="color" placeholder={'TaskColorInput'} onChange={changeColor}/>
    </Fragment>
  )
};

TaskColorInput.propTypes = {
  changeColor: PropTypes.func
};

export default TaskColorInput;