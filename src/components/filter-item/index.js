import React from 'react';
import PropTypes from 'prop-types';

const FilterItem = (props) => {

  const { text, status, onFilter, filterActive } = props;

  return (
    <li className={'task__filter-item ' + (filterActive === status ? "selected" : '') }>
      <a href={'#/' + status} onClick={onFilter.bind(null, status)}>{text}</a>
    </li>
  )
};

FilterItem.propTypes = {
  text: PropTypes.string,
  status: PropTypes.string,
  onFilter: PropTypes.func,
  filterActive: PropTypes.string
};

export default FilterItem