import React from 'react';
import PropTypes from 'prop-types';
import FilterItem from '../filter-item';
import FilterName from '../../constants/task-filter';

const TaskFilter = (props) => {

  const {filterActive, onFilter, arrLength, completedItem, deleteComplete  } = props;

  let filterItems = FilterName.map((filterName) => {
    return <FilterItem key={filterName.id} {...filterName} onFilter={onFilter} filterActive={filterActive}/>
  });

  return (
    <div className='task__filter-list-wrap'>
      <span>{arrLength  + (arrLength === 1 ? ' item, ' : ' items, ')}</span>
      <span>{completedItem + ' completed, '}</span>
      <span>{arrLength-completedItem + ' active. '}</span>

      <ul className='task__filter-list'>
        {filterItems}
        <li className='task__filter-item '>{completedItem ? <a href="#/" onClick={deleteComplete}>Clear complete</a> : null}</li>
      </ul>
    </div>
  )
};

TaskFilter.propTypes = {
  filterActive: PropTypes.string,
  onFilter: PropTypes.func,
  arrLength: PropTypes.number,
  completedItem: PropTypes.number,
  deleteComplete: PropTypes.func

};

export default TaskFilter