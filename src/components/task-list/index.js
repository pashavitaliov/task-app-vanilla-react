import React from 'react';
import ListItem from '../list-item';

const TaskList = (props) => {

  const { tasks } = props;

  return (
    <div className='task__list-wrap'>

      <ul className='task__list'>
        {tasks.map((task) => {
          return <ListItem {...props} key={task.id} {...task}/>
        })}
      </ul>

    </div>
  )
};

export default TaskList